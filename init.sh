#!/usr/bin/env bash

extranetRoot=~/.rifextranet

mkdir -p "$extranetRoot"

cp -i src/stubs/Extranet.yaml "$extranetRoot/Extranet.yaml"
cp -i src/stubs/after.sh "$extranetRoot/after.sh"
cp -i src/stubs/aliases "$extranetRoot/aliases"

echo "ExtranetDebian initialisé!"
