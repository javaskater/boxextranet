# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION = "2"
confDir = $confDir ||= File.expand_path("~/.rifextranet")

extranetYamlPath = confDir + "/Extranet.yaml"
afterScriptPath = confDir + "/after.sh"
aliasesPath = confDir + "/aliases"

require File.expand_path(File.dirname(__FILE__) + '/scripts/extranet.rb')

Vagrant.require_version '>= 1.8.4'

Vagrant::Config(AGRANTFILE_API_VERSION).run do |config|
  # This Vagrantfile is auto-generated by `vagrant package` to contain
  # the MAC address of the box. Custom configuration should be placed in
  # the actual `Vagrantfile` in this box.
  #config.vm.base_mac = "080027294DBB"
  if File.exist? aliasesPath then
    config.vm.provision "file", source: aliasesPath, destination: "~/.bash_aliases"
  end

  if File.exist? extranetYamlPath then
    settings = YAML::load(File.read(extranetYamlPath))
  end

  Extranet.configure(config, settings)

  if File.exist? afterScriptPath then
    config.vm.provision "shell", path: afterScriptPath, privileged: false
  end

  if defined? VagrantPlugins::HostsUpdater
    config.hostsupdater.aliases = settings['sites'].map { |site| site['map'] }
  end
end

# Load include vagrant file if it exists after the auto-generated
# so it can override any of the settings
include_vagrantfile = File.expand_path("../include/_Vagrantfile", __FILE__)
load include_vagrantfile if File.exist?(include_vagrantfile)
